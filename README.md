# simpletextviewimageview

This project demonstrates use of the 2 basic elements in Mobile app development i.e TextView and ImageView.

Additionally the project focuses on adding images in app via 2 sources namely:

1. Network Image. (External Url)
2. Asset Image. (Image present inside the project)
It also shows how to add icons in the project such as the app logo and use the Material design package while developing apps.

 ![layout](app_images/layout.png)