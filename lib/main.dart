import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: Text('Welcome to the Mobile World!!'),
        backgroundColor: Colors.blue[900],
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          children: const [
            Image(
              image: AssetImage("app_images/apple.png"),
            ),
            Image(
              image: NetworkImage(
                  'https://pngimg.com/uploads/android_logo/android_logo_PNG28.png'),
            ),
            Padding(
              padding: EdgeInsets.all(50.0),
              child: Text(
                'Which platform do you prefer?',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.blueGrey,
                    fontSize: 20.0),
              ),
            ),
          ],
        ),
      ),
    ),
  ));
}
